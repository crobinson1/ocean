/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 3008517680
 * @format
 * @flow
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  TextInput,
  ScrollView,
  View,
  Text,
  StatusBar,
  AsyncStorage,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  Modal,
  RefreshControl,
  Alert,
  Dimensions,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import styles from "./styles.css.js";
import { Input, Button, ListItem, Header, Avatar, Icon } from 'react-native-elements';
import Parse from "parse/react-native";
Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(
  "myAppIdOceanCr0b",
  "crobskey",
  "b0t3c0m4st3rk3y"
);
Parse.serverURL = 'https://api.unidivisas.com/ocean';
const Inicio: () => React$Node = () => {
  const [user, setUser] = useState( null );
  const [filtrotext, setFiltrotext] = useState("");
  const [password, setPassword] = useState("");
  const [telefono, setTelefono] = useState("");
  const [nombre, setNombre] = useState("");
  const [filtro, setFiltro] = useState("");
  const [apellido, setApellido] = useState("");
  const [titleNombre, setTitleNombre] = useState("Nombre")
  const [titleTel, setTitleTel] = useState("Teléfono")
  const [titleEmail, setTitleEmail] = useState("Email")
  const [titleCodigo, setTitleCodigo] = useState("Código")
  const [isLoading, setLoading] = useState(true);
  const [tags, setTags] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    setLoading(true);
    AsyncStorage.getItem('usuario', (err, result) => {
      setUser(JSON.parse(result))        
      setLoading(false);
      fetchTags();
    });
  }, []);
  
  const fetchTags = async()=>{
    const GameScore = Parse.Object.extend("tagsOcean");
    const query = new Parse.Query(GameScore);
    query.ascending("nombre");
    const results = await query.find();
    setTags(results)
  }
  const selectData = async(item)=>{
      const GameScore = Parse.Object.extend("tagsOcean");
      const query = new Parse.Query(GameScore);
      query.equalTo("nombre", item.get('nombre'));
      query.equalTo("email", item.get('email'));
      query.ascending("nombre");
      const results = await query.find();
      setSelected(results)
      setModal2Visible(true)
  }

  const filtrar = async() => {
    setLoading(true)
    setModalVisible(false)
    if(filtrotext && filtrotext!="" && filtrotext.length){
      switch (filtro) {
        case "nombre":
            setTitleNombre(filtrotext)
          break;
        case "telefono":
            setTitleTel(filtrotext)
          break;
        case "email":
            setTitleEmail(filtrotext)
          break;
        case "codigo":
            setTitleCodigo(filtrotext)
          break;
        default:
          break;
      }
      const GameScore = Parse.Object.extend("tagsOcean");
      const query = new Parse.Query(GameScore);
      query.fullText(filtro, filtrotext);      
      
      const results = await query.find();
      setTags(results)
    }else{
      fetchTags()
      setTitleNombre("Nombre")
      setTitleTel("Teléfono")
      setTitleEmail("Email")
      setTitleCodigo("Código")
    }
    setLoading(false)
  }
  let nameTemp = null
  if(isLoading)
    return(
        <ActivityIndicator />
    )
  else
    return (
    <>
      <Header
        containerStyle={{ backgroundColor: "#E7BB2E", height: 95 }}
        centerComponent={ <Text style={styles.headerTitle}>Mis Tags</Text>}
      />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            {/* <View style={styles.userHeader}>
            <ImageBackground source={require('./assets/barra.png')} style={styles.barra}>
              <Text>{user.nombre} {user.apellido}</Text>
            </ImageBackground>
            </View> */}
            
            <View style={styles.sectionContainer}>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: "space-around", marginTop: 20}}>
                <Button
                containerStyle={{width: 70, height: 40}}
                buttonStyle={{borderColor:"#808080"}}
                titleStyle={{fontSize: 12,color:"#808080"}}
                type="outline"
                icon={
                    <Icon
                    name="search"
                    size={15}
                    color="#808080"
                    />
                }
                title={titleNombre}
                onPress={()=>{ setModalVisible(true); setFiltro("nombre"); }}
                />

<Button
                containerStyle={{width: 70, height: 40}}
                buttonStyle={{borderColor:"#808080"}}
                titleStyle={{fontSize: 12,color:"#808080"}}
                type="outline"
                icon={
                    <Icon
                    name="search"
                    size={15}
                    color="#808080"
                    />
                }
                title="Teléfono"
                onPress={()=>{ setModalVisible(true); setFiltro("telefono"); }}
                />

<Button
                containerStyle={{width: 70, height: 40}}
                buttonStyle={{borderColor:"#808080"}}
                titleStyle={{fontSize: 12,color:"#808080"}}
                type="outline"
                icon={
                    <Icon
                    name="search"
                    size={15}
                    color="#808080"
                    />
                }
                title="Email"
                onPress={()=>{ setModalVisible(true); setFiltro("email"); }}
                />

<Button
                containerStyle={{width: 70, height: 40}}
                buttonStyle={{borderColor:"#808080"}}
                titleStyle={{fontSize: 12,color:"#808080"}}
                type="outline"
                icon={
                    <Icon
                    name="search"
                    size={15}
                    color="#808080"
                    />
                }
                title="Código"
                onPress={()=>{ setModalVisible(true); setFiltro("codigo"); }}
                />
            </View>
              <View style={styles.rowIcon}>
              {tags.map((item, index) => {
                  if(item.get('nombre') !== nameTemp){
                    nameTemp = item.get('nombre')
                    return (
                      <View style={{alignItems: "center", width: "25%", justifyContent: "center"}}>
                          <TouchableOpacity onPress={()=>selectData(item)}>
                          <View style={styles.blackSquare}>
                              <Image source={require('./assets/logo.png')} style={styles.miniLogo}  />
                          </View>
                          </TouchableOpacity>
                          <Text style={styles.miniText}>{item.get("nombre")}</Text>
                      </View>
                    );
                  }
                  return null
                  
                })}
              </View>
            </View>
          </View>
        </ScrollView>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={modalVisible}
        >
          <Header
                containerStyle={{ backgroundColor: "#E7BB2E", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Buscar por {filtro}</Text>}
                rightComponent={<Icon
                  clean
                  size = {40}
                  name='md-close'
                  type='ionicon'
                  onPress={() => setModalVisible(false)} />}
              />
          
          <ScrollView>
          <KeyboardAvoidingView 
              style={styles.container} 
              // keyboardVerticalOffset = {Header.HEIGHT + 20} // adjust the value here if you need more padding
              //style = {{ flex: 1 }}
              behavior = "padding" 
            >
                <Input
                    value = {filtrotext}
                    autoCapitalize = "none"
                    onChangeText={texto => setFiltrotext( texto )}
                    placeholder="..."
                    inputContainerStyle={styles.inputContainer}  
                    inputStyle={styles.inputStyle}
                    errorStyle={styles.errorInputStyle}
                    autoCorrect={false}
                    blurOnSubmit={false}
                    placeholderTextColor="#7384B4"
                />
                <Button
                loading={isLoading}
                title="Buscar"
                containerStyle={{ flex: -1 }}
                buttonStyle={styles.signUpButton}
                titleStyle={styles.signUpButtonText}
                onPress={() => filtrar() }
                disabled={isLoading}
                />
            </KeyboardAvoidingView>
          </ScrollView>
        </Modal>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={modal2Visible}
        >
          <Header
                containerStyle={{ backgroundColor: "#E7BB2E", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Tag</Text>}
                rightComponent={<Icon
                  clean
                  size = {40}
                  name='md-close'
                  type='ionicon'
                  onPress={()=> setModal2Visible(false) } />}
              />
          
          <ScrollView>
                <View style={styles.body}>
                    {selected.map((item, index)=>(
                      <View key={index + 'modal'} style={[
                        styles.sectionContainer, 
                        {
                            backgroundColor: "black", 
                            width: "80%", 
                            marginHorizontal: "10%",
                            padding: 30
                        }]}>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Nombre: 
                            {item ? (" " + item.get("nombre")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Teléfono:
                            {item ? (" " + item.get("telefono")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Correo electrónico:
                            {item ? (" " + item.get("email")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Código:
                            {item ? (" " + item.get("codigo")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Información adicional:
                            {item ? (" " + item.get("info")) : (null)}</Text>
                        </Text>
                    </View>
                    ))}
                </View>
                
                <Button
                  title="Cerrar"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ setModal2Visible(false) }}
                />
            
          </ScrollView>
        </Modal>
      </SafeAreaView>
    </>
  );
  };

export default Inicio;