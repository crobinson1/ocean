/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 3008517680
 * ccb.org.co
 * @format
 * @flow
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  TextInput,
  ScrollView,
  View,
  Text,
  StatusBar,
  AsyncStorage,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  Modal,
  RefreshControl,
  Alert,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  Linking
} from 'react-native';
import styles from "./styles.css.js";
import { Input, Button, ListItem, Header, Avatar, Icon } from 'react-native-elements';
import NfcManager, { NfcTech, NfcEvents } from 'react-native-nfc-manager';
import Parse from "parse/react-native";
const { width, height } = Dimensions.get("window");
const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;
Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(
  "myAppIdOceanCr0b",
  "crobskey",
  "b0t3c0m4st3rk3y"
);
Parse.serverURL = 'https://api.unidivisas.com/ocean';
const Crear: () => React$Node = () => {
  const [text, setText] = useState( null );
  const [codigo, setCodigo] = useState( null );
  const [user, setUser] = useState( null );
  const [email, setEmail] = useState(null);
  const [informacion, setInformacion] = useState(null);
  const [nombre, setNombre] = useState(null);
  const [telefono, setTelefono] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [isShowing, setShowing] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const cleanUp = () => {
        NfcManager.cancelTechnologyRequest().catch(() => 0);
    }

    const crear = async()=>{

        if(nombre && telefono && email && text && informacion){
            const GameScore = Parse.Object.extend("tagsOcean");
            const gameScore = new GameScore();
            gameScore.set("nombre", nombre);
            gameScore.set("telefono", telefono);
            gameScore.set("email", email);
            gameScore.set("codigo", text);
            gameScore.set("info", informacion);
            gameScore.set("usuarioId", codigo);
            await gameScore.save()
            Alert.alert('🔥', 'Datos guardados con éxito.');
            setNombre(null)
            setTelefono(null)
            setEmail(null)
            setInformacion(null)
            setModalVisible(false)
        }else{
            Alert.alert('🔥', 'Debes ingresar todos los datos.');
        }
    }

  const _test = async () => {
    /**
     * Pruebas para llamar o enviar correo
     */
    setModalVisible(true)
    /**
     * *************************************
     */

    setNombre( null )
    setEmail( null )
    setTelefono( null )
    setInformacion( null )
    setCodigo( null )
    setShowing( false )
    try {
      await NfcManager.registerTagEvent();
    } catch (ex) {
      console.warn('ex', ex);
      NfcManager.unregisterTagEvent().catch(() => 0);
    }
  }

  const readData = async () => {
    setModalVisible(true)
    try {
        let tech = Platform.OS === 'ios' ? NfcTech.MifareIOS : NfcTech.NfcA;
        alert(cmd)
        let resp = await NfcManager.requestTechnology(tech, {
            alertMessage: "Ready for magic"
        });
        // alert(resp)
        let cmd = Platform.OS === 'ios' ? NfcManager.sendMifareCommandIOS : NfcManager.transceive;
        // alert(cmd)
        resp = await cmd([0x3A, 4, 4])
        let payloadLength = parseInt(resp.toString().split(",")[1]);
        let payloadPages = Math.ceil(payloadLength / 4);
        let startPage = 5;
        let endPage = startPage + payloadPages - 1;
        resp = await cmd([0x3A, startPage, endPage]);
        let bytes = resp.toString().split(",");
        let text = ""
        for(let i=0; i<bytes.length; i++){
            if (i<5){
                continue;
            }
            if (parseInt(bytes[i]) === 254){
                break;
            }
            text = text + String.fromCharCode(parseInt(bytes[i]));
        }
        setText(text)
        setModalVisible(true)
    } catch(err){
        setText(err.toString())
        setModalVisible(true)
        
        cleanUp();
    }
}
  
  useEffect(() => {
    NfcManager.start();
    NfcManager.setEventListener(NfcEvents.DiscoverTag, tag => {
      // alert(JSON.stringify(tag))
      setText(tag.id)
      /*
       * busco si ya existe!
       */
      const GameScore = Parse.Object.extend("tagsOcean");
      const query = new Parse.Query(GameScore);
      query.equalTo("codigo", tag.id);
      setShowing( false )
      query.find().then(function(results) {
        // results has the list of groups with role x
        const result = results[0]
        setNombre( result.get('nombre') )
        setEmail( result.get('email') )
        setTelefono( result.get('telefono') )
        setInformacion( result.get('info') )
        setCodigo( result.get('usuarioId') )
        setShowing( true )
     })
      
      setModalVisible(true)
      NfcManager.setAlertMessageIOS('I got your tag!');
      NfcManager.unregisterTagEvent().catch(() => 0);
    });
  }, []);
  
  if(isLoading)
    return(
        <ActivityIndicator />
    )
  else
    return (
    <>
      <Header
        containerStyle={{ backgroundColor: "#E7BB2E", height: 95 }}
        centerComponent={ <Text style={styles.headerTitle}>Escanear código</Text>}
      />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}>
        <View style={{width: SCREEN_WIDTH, height: SCREEN_HEIGHT/2}} />
        <View style={{width: SCREEN_WIDTH, height: SCREEN_HEIGHT/3, justifyContent: "center", backgroundColor: 'black', borderTopEndRadius: 30, borderTopStartRadius: 30}}>
            <TouchableOpacity onPress={()=>_test()}>
            <ImageBackground style={{
                alignSelf: "center",
                width: 100,
                height: 100
            }} source={require('./assets/iconcell.png')} ></ImageBackground>
            </TouchableOpacity>
        </View>    
      </View>
        </ScrollView>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={modalVisible}
        >
          <Header
                containerStyle={{ backgroundColor: "#E7BB2E", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Crear cuenta</Text>}
                rightComponent={<Icon
                  clean
                  size = {40}
                  name='md-close'
                  type='ionicon'
                  onPress={() => setModalVisible(false)} />}
              />
          
          <ScrollView>
            <KeyboardAvoidingView 
                style={{flex: 1}} 
                behavior = "padding" 
                >
              <View style={styles.sectionContainer}>
              <Input
                value = {text}
                onChangeText={texto => setText( texto )}
                placeholder="Código NFC"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="#836514"
              />
              <Input
                value = {codigo}
                onChangeText={texto => setCodigo( texto )}
                placeholder="Código de Usuario"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="#836514"
              />
              <Input
                value = {nombre}
                onChangeText={texto => setNombre( texto )}
                placeholder="Nombre"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="#836514"
              />
              <Input
                value = {email}
                autoCapitalize = "none"
                keyboardType = "email-address"
                onChangeText={texto => setEmail( texto )}
                placeholder="Correo electrónico"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="#836514"
              />
              {isShowing ? (
                <Button
                  title="Enviar email"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ Linking.openURL(`mailto:${email}`) }}
                  icon={
                    <Icon
                      name="mail"
                      size={35}
                    />
                  }
                />
              ) : null}
              <Input
                value = {telefono}
                keyboardType="phone-pad"
                onChangeText={texto => setTelefono( texto )}
                placeholder="Teléfono"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="#836514"
              />
              {isShowing ? (
                <Button
                  title="Llamar"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ Linking.openURL(`tel:${telefono}`) }}
                  icon={
                    <Icon
                      name="phone"
                      size={35}
                    />
                  }
                />
              ) : null}
              <Input
                value = {informacion}
                onChangeText={texto => setInformacion( texto )}
                placeholder="Información adicional"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="#836514"
              />
              {!isShowing ? (
                <Button
                loading={isLoading}
                title="¡Crear!"
                containerStyle={{ flex: -1, alignSelf: "center" }}
                buttonStyle={styles.signUpButton}
                titleStyle={styles.signUpButtonText}
                onPress={crear}
                disabled={isLoading}
                />
              ) : null}
                <Button
                  title="Cerrar"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ setModalVisible(false) }}
                />
            </View>
            </KeyboardAvoidingView>
          </ScrollView>
        </Modal>
      </SafeAreaView>
    </>
  );
  };

export default Crear;