import { StyleSheet, Dimensions } from "react-native";
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
  ActivityIndicator,
  Platform
} from 'react-native/Libraries/NewAppScreen';
const { width, height } = Dimensions.get("window");
const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;
const IMAGE_SIZE = SCREEN_WIDTH - 80;

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    // width: SCREEN_WIDTH,
    // height: SCREEN_HEIGHT,
    alignItems: "center",
    justifyContent: "flex-start",
    fontFamily: "Helvetica Neue"
  },
  containerPopup: {
    width: "80%",
    alignItems: "center",
    justifyContent: "space-around",
    fontFamily: "Helvetica Neue"
  },
  formContainer: {
    flex: 1,
    justifyContent: "space-around",
  },
  loginTitle: {
      marginVertical: 32,
      color: "white", // color: "#E7BB2E",
      fontSize: 30,
      fontWeight: "200"
  },
  signUpText: {
    color: "white",
    fontSize: 28,
    fontFamily: "light",
  },
  whoAreYouText: {
    color: "#7384B4",
    fontFamily: "bold",
    fontSize: 14,
  },
  userTypesContainer: {
    marginTop: 60,
    flexDirection: "row",
    justifyContent: "space-around",
    width: SCREEN_WIDTH,
    alignItems: "center",
  },
  mark: {
      alignItems: "center",
      width: 183,
      height: 171
  },
  userTypeItemContainer: {
    alignItems: "center",
    justifyContent: "center",
    opacity: 0.5,
  },
  userTypeItemContainerSelected: {
    opacity: 1,
  },
  userTypeMugshot: {
    margin: 4,
    height: 70,
    width: 70,
  },
  userTypeMugshotSelected: {
    height: 100,
    width: 100,
  },
  userTypeLabel: {
    color: "yellow",
    fontFamily: "bold",
    fontSize: 11,
  },
  inputContainer: {
    paddingLeft: 8,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#DDD',
    height: 45,
    marginVertical: 10,
  },
  textareaContainer: {
    paddingLeft: 8,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#DDD',
    height: "auto",
    marginVertical: 10,
  },
  inputStyle: {
    flex: 1,
    marginLeft: 10,
    color: 'white',
    fontSize: 16,
  },
  inputStyleBlack: {
    flex: 1,
    marginLeft: 10,
    color: 'black',
    fontSize: 16,
  },
  errorInputStyle: {
    marginTop: 0,
    textAlign: "center",
    color: "#F44336",
  },
  signUpButtonText: {
    fontSize: 20,
    fontWeight: "200",
    color: "white",
  },
  signUpButton: {
    backgroundColor: "gray",
    width: 250,
    borderRadius: 40,
    marginVertical: 20
  },
  clearButton: {
    color: "white", // color: "#E7BB2E",
    fontSize: 20,
    fontWeight: "200"
  },
  loginHereContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  alreadyAccountText: {
    fontFamily: "lightitalic",
    fontSize: 12,
    color: "white",
  },
  loginHereText: {
    color: "#FF9800",
    fontFamily: "lightitalic",
    fontSize: 12,
  },
  statusBar: {
    height: 10
  },
  navBar: {
    height: 60,
    width: SCREEN_WIDTH,
    justifyContent: "center",
    alignContent: "center"
  },
  nameHeader: {
    color: "white",
    fontSize: 22,
    textAlign: "center"
  },
  infoTypeLabel: {
    fontSize: 15,
    textAlign: "right",
    color: "rgba(126,123,138,1)",
    fontFamily: "regular",
    paddingBottom: 10
  },
  infoAnswerLabel: {
    fontSize: 15,
    color: "white",
    fontFamily: "regular",
    paddingBottom: 10
  },
  buttonMoradoLabel: {
    fontSize: 15,
    color: "white",
    fontFamily: "regular"
  },
  buttonMorado: {
    backgroundColor: "#622d7a",
    borderRadius: 100,
    width: 127
  },
  engine: {
        position: 'absolute',
        right: 0,
      },
      body: {
        
      },
      sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
      },
      sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: "#E7BB2E",
      },
      headerTitle: {
        fontSize: 24,
        fontWeight: '200',
        color: "white",
      },
      sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
      },
      highlight: {
        fontWeight: '700',
      },
      footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
      },
      bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
      },
      preview: {
        flex: 1,
        height: SCREEN_HEIGHT/3,
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
      blackscreen: {
          backgroundColor: "black"
      },
      userHeader: {
        margin: 20,
        height: 200,
        borderColor: "black",
        borderWidth: 1,
        borderRadius: 30,
        width: SCREEN_WIDTH - 40,
        alignSelf: "center"
    },
    imgHeader: {
      height: 200,
      borderColor: "black",
      borderWidth: 1,
      borderRadius: 30,
      width: SCREEN_WIDTH - 40,
      alignSelf: "center"
    },
    barra: {
        width: SCREEN_WIDTH/2, 
        height : SCREEN_WIDTH*56/(2*359), 
        alignSelf: 'flex-end',
        marginTop: 130,
        alignItems: 'center',
        justifyContent: 'center'
    },
    blackSquare: {
        width: 50, 
        height: 50, 
        backgroundColor: 'black', 
        borderRadius: 10,
        padding: 10
    },
    miniLogo: {
        width: 30, 
        height: 30,
        alignSelf: "center"
    },
    miniText: { fontSize: 10, fontWeight: "bold", alignSelf: "center" },
    rowIcon: {
      marginTop: 14,
      flexDirection:"row",
      flexWrap: 'wrap'
    },
    scrollView: {height: SCREEN_HEIGHT, backgroundColor: 'transparent'},
    fixed: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0
    },
    imgContainter: {
      width: Dimensions.get("window").width, //for full screen
      height: Dimensions.get("window").height //for full screen
    },
        
});
export default styles;
