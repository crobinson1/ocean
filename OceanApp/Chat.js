import React, { Component } from 'react';
import { 
    View, Text, AsyncStorage, ImageBackground, SafeAreaView, ScrollView, Modal, Image, RefreshControl 
} from 'react-native';
import { Input, Button, ListItem, Header, Avatar, Icon } from 'react-native-elements';
import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat'
import styles from "./styles.css.js";
import Parse from "parse/react-native";
import PushNotification from 'react-native-push-notification';


export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
        partner: null,
        currenuser: null,
        chatTitle: null,
        lastFecha: null,
        fontLoaded: false,
        appIsReady: true,
        isTyping: false,
        setModalAddChat: false,
        setModalChat: false,
        tags: [],
        list: [],
        messages: [],
    };
  }

  onSend(messages = []) {
    const partner = this.state.partnerObj
    const self = this
    for (let index = 0; index < messages.length; index++) {
        const mensaje = messages[index];
        const GameScore = Parse.Object.extend("Chat");
        const gameScore = new GameScore();
        
        gameScore.set("senderId", self.state.currenuser.objectId);
        gameScore.set("receiverId", self.state.partner);
        gameScore.set("mensaje", mensaje.text);
        
        gameScore.save()
        .then((gameScore) => {
            self.fetchMessages(self.state.partner)
          }, (error) => {
            
        });

        /**
         * Notify the sent message
         */
        if(partner.plataforma){
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("X-Parse-Application-Id", "myAppIdDestExpCr0b");
            myHeaders.append("X-Parse-REST-API-Key", "b0t3c0r3stk3y");
            myHeaders.append("X-Parse-javascript-Key", "crobskey");

            let raw = JSON.stringify({
                "regTokens":[partner.token],
                "cuerpo":mensaje.text,
                "titulo":partner.nombre + partner.apellido,
                "plataforma":partner.plataforma,
                "sender":partner.objectId});
            let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
            };
            fetch("https://api.unidivisas.com/parse/functions/notificarOcean", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
        }
    }
    
    // this.setState(previousState => ({
    //   messages: GiftedChat.append(previousState.messages, messages),
    // }))
  }

  fetchReadMessages = async() => {
    const self = this
    const userId = self.state.partner

    if(userId){
        /**
         * Traigo mensajes y después guardo la fecha del último para poder traer nuevos
         */ 
        const query1 = new Parse.Query("Chat");
        query1.equalTo("senderId", self.state.currenuser.objectId);
        query1.equalTo("receiverId", userId);
        if(self.state.lastFecha)
            query1.greaterThan("createdAt", self.state.lastFecha);
        // const res1 = await query1.find()
        // alert(res1.length)
        const query2 = new Parse.Query("Chat");
        query2.equalTo("senderId", userId);
        query2.equalTo("receiverId", self.state.currenuser.objectId);
        if(self.state.lastFecha)
            query2.greaterThan("createdAt", self.state.lastFecha);
        
        
        const mainQuery = Parse.Query.or(query1, query2)
        
        const results = mainQuery.find().then(function(results) {
            
            for (let index = 0; index < results.length; index++) {
                const obj = results[index];
                self.setState(previousState => ({
                    messages: GiftedChat.append(previousState.messages, [
                        {
                            _id: obj.id,
                            text: obj.get("mensaje"),
                            createdAt: obj.createdAt,
                            user: {
                                _id: obj.get("senderId")===userId ? 2 : 1,
                                name: 'React Native',
                                avatar: 'https://placeimg.com/140/140/any',
                            },
                        }
                    ]),
                }), self.setState({lastFecha: obj.createdAt}))   
            }
        })
        .catch(function(error) {
            // There was an error.
            alert(JSON.stringify(error.message))
        });
    }
  }

  fetchMessages = async(userId) => {
    const self = this
    /**
     * Traigo mensajes y después guardo la fecha del último para poder traer nuevos
     */ 
    const query1 = new Parse.Query("Chat");
    query1.equalTo("senderId", self.state.currenuser.objectId);
    query1.equalTo("receiverId", userId);
    if(this.state.lastFecha)
        query1.greaterThan("createdAt", this.state.lastFecha);
    // const res1 = await query1.find()
    // alert(res1.length)
    const query2 = new Parse.Query("Chat");
    query2.equalTo("senderId", userId);
    query2.equalTo("receiverId", self.state.currenuser.objectId);
    if(this.state.lastFecha)
        query2.greaterThan("createdAt", this.state.lastFecha);
    
    
    const mainQuery = Parse.Query.or(query1, query2)
    
    mainQuery.find().then(function(results) {
        for (let index = 0; index < results.length; index++) {
            const obj = results[index];
            self.setState(previousState => ({
                messages: GiftedChat.append(previousState.messages, [
                    {
                        _id: obj.id,
                        text: obj.get("mensaje"),
                        createdAt: obj.createdAt,
                        user: {
                            _id: obj.get("senderId")===userId ? 2 : 1,
                            name: 'React Native',
                            avatar: 'https://placeimg.com/140/140/any',
                        },
                    }
                ]),
            }), self.setState({lastFecha: obj.createdAt}))   
        }
    })
    .catch(function(error) {
        // There was an error.
        alert(JSON.stringify(error.message))
    });
  }

  fetchMyChats = async() => {
    const self = this
    let nameTemp = null
    let mypartners = []
    let list = []
    const query1 = new Parse.Query("Chat");
    query1.equalTo("senderId", self.state.currenuser.objectId);
    const query2 = new Parse.Query("Chat");
    query2.equalTo("receiverId", self.state.currenuser.objectId);
    const mainQuery = Parse.Query.or(query1, query2)
    const results = await mainQuery.find()
    for (let index = 0; index < results.length; index++) {
        const obj = results[index];
        if(obj.get("senderId")===self.state.currenuser.objectId){
            if(mypartners.indexOf(obj.get("receiverId"))===-1)
                mypartners.push(obj.get("receiverId"))
        }else{
            if(mypartners.indexOf(obj.get("senderId"))===-1)
                mypartners.push(obj.get("senderId"))
        }
    }
    // alert(JSON.stringify(mypartners))
    
    for (let index = 0; index < this.state.tags.length; index++) {
        const l = this.state.tags[index];
        if(l.get('nombre') !== nameTemp){
            nameTemp = l.get('nombre')
            if(mypartners.indexOf(l.id)!==-1){
                list.push(
                    {
                        obj: l,
                        name: l.get('nombre'),
                        avatar_url: '',
                        subtitle: ''
                    }
                )
            }
        }
        
    }
    self.setState({list})
  }
  
  setChat = async (userObj) => {
    const GameScore = Parse.Object.extend("UserOcean");
    const query = new Parse.Query(GameScore);
    query.equalTo("objectId", userObj.id);  // find all the women
    const women = await query.find();
      this.setState({
        messages: [],
        setModalAddChat: false,
        chatTitle: userObj.get("nombre"),
        partner: userObj.id,
        partnerObj: women[0].toJSON(),
        lastFecha: null
      })
      
      this.fetchMessages(userObj.get("usuarioId"))
      this.setState({
        setModalChat: true
      })

  }


  fetchTags = async () => {
    this.setState({
        fontLoaded: false
    })
    const GameScore = Parse.Object.extend("UserOcean");
    const query = new Parse.Query(GameScore);
    query.ascending("nombre");
    const results = await query.find();
    this.setState({tags: results})
    this.setState({
        fontLoaded: true
    }, ()=> this.fetchMyChats())
  }

  componentDidMount = () => {
    const self = this
    AsyncStorage.getItem('usuario', (err, result) => {
        const usuario = JSON.parse(result)
        console.log(usuario.objectId)
        PushNotification.configure({
            onRegister: function(token) {
                console.log("TOKEN:", token);
                AsyncStorage.setItem('TOKEN', token.token);
                const query = new Parse.Query(Parse.Object.extend("UserOcean"))
                query.get(usuario.objectId)
                .then((gameScore) => {
                    gameScore.set("token", token.token)
                    gameScore.set("plataforma", token.os)
                    gameScore.save()
                }, (error) => {
                    console.log(error)
                });
            },        
            onNotification: function(notification) {
              console.log("NOTIFICATION:", notification);
            //   if(notification.sender === self.state.partner)
            //     self.fetchReadMessages()
              // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
            //   notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
          
            // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
            senderID: "137927602251",
          
            // IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
              alert: true,
              badge: true,
              sound: true
            },
            popInitialNotification: true,
            requestPermissions: true
          });

        this.setState({
            fontLoaded: true,
            currenuser: JSON.parse(result)
        }, ()=>{
            this.fetchTags()
        })
    });
    
    setInterval( () => {
        this.fetchReadMessages();
        this.fetchMyChats()
     },5000);
  }
  render() {
      const {list, setModalAddChat, setModalChat, tags, chatTitle} = this.state
      let nameTemp = null
    return (
        <View style={{ flex: 1 }}>
            <Header
                containerStyle={{ backgroundColor: "black", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Chat</Text>}
            />
            <SafeAreaView>
                <ImageBackground
                    style={[styles.fixed, styles.imgContainter]}
                    source={require('./assets/fondo.png')}
                />
                <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                    <View style={styles.body}>
                        {
                            list.map((l, i) => (
                            <ListItem
                                key={i}
                                leftAvatar={
                                    <View style={styles.blackSquare}>
                                        <Image source={require('./assets/logo.png')} style={styles.miniLogo}  />
                                    </View>
                                }
                                title={l.name}
                                onPress={() => this.setChat(l.obj)}
                                bottomDivider
                            />
                            ))
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
            <Modal
                animationType={'slide'}
                transparent={false}
                visible={setModalAddChat}
            >
                <Header
                    containerStyle={{ backgroundColor: "black", height: 95 }}
                    centerComponent={ <Text style={styles.headerTitle}>Selecciona</Text>}
                    rightComponent={<Icon
                    clean
                    size = {40}
                    name='md-close-circle'
                    type='ionicon'
                    color="#ccc"
                    onPress={() => this.setState({setModalAddChat: false})} />}
                />
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}
                    refreshControl={
                        <RefreshControl
                          refreshing={!this.state.fontLoaded}
                          onRefresh={this.fetchTags}
                        />
                    }
                >
                    <View style={styles.body}>
                {
                    tags.map((l, i) =>{
                        if(l.get('nombre') !== nameTemp){
                            nameTemp = l.get('nombre')
                            return  (
                                <ListItem
                                    key={i}
                                    leftAvatar={
                                        <View style={styles.blackSquare}>
                                            <Image source={require('./assets/logo.png')} style={styles.miniLogo}  />
                                        </View>
                                    }
                                    title={l.get('nombre')}
                                    onPress={() => this.setChat(l)}
                                    bottomDivider
                                />
                            )
                        }
                    })
                }
                    </View>
                </ScrollView>
                
            </Modal>
            <Modal
                animationType={'slide'}
                transparent={false}
                visible={setModalChat}
            >
                <Header
                    containerStyle={{ backgroundColor: "black", height: 95 }}
                    centerComponent={ <Text style={styles.headerTitle}>{chatTitle}</Text>}
                    rightComponent={<Icon
                    clean
                    size = {40}
                    name='md-close-circle'
                    type='ionicon'
                    color="#ccc"
                    onPress={() => this.setState({setModalChat: false, partner: null})} />}
                />
                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                    _id: 1,
                    }}
                    timeTextStyle={{ left: { color: 'gray' }, right: { color: 'white' } }}
                    isTyping={this.state.isTyping}
                    renderSend = { props => (
                        <Send
                        {...props}
                        >
                            <View style={{marginRight: 10, marginBottom: 5, flexDirection: "row"}}>
                                <Icon
                                    clean
                                    size = {20}
                                    name='md-send'
                                    type='ionicon'
                                    color="black"
                                />
                                <Text style={{fontWeight: "bold"}}> Enviar</Text>
                            </View>
                        </Send>
                        ) }
                    renderBubble = {
                        props => (
                            <Bubble
                        {...props}
                        wrapperStyle={{
                          right: {
                            backgroundColor: "gray"
                          }
                        }}
                      />
                        )
                    }
                />
                {/* <KeyboardAvoidingView behavior="padding" /> */}
                {/* <KeyboardSpacer /> */}
                {/* {Platform.OS === 'android' ? <KeyboardSpacer /> : null } */}
            </Modal>
        </View>
    );
  }
}
