/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  TextInput,
  ScrollView,
  View,
  Text,
  StatusBar,
  AsyncStorage,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  Modal,
  RefreshControl,
  Alert,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';

import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import styles from "./styles.css.js";
import { Input, Button, ListItem, Header, Avatar, Icon } from 'react-native-elements';
import Parse from "parse/react-native";
import AppNavigator from './AppNavigator';
Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(
  "myAppIdOceanCr0bs",
  "crobskey",
  "b0t3c0m4st3rk3y"
);
Parse.serverURL = 'https://api.unidivisas.com/ocean';

const App = () => {
  const [user, setUser] = useState( null );
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [telefono, setTelefono] = useState("");
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [loaded, setLoaded] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [selected, setSelected] = useState(null);
  const [tags, setTags] = useState([]);
  useEffect(() => {
    setLoading(false);
    AsyncStorage.getItem('usuario', (err, result) => {
      setUser(JSON.parse(result))
      fetchTags(JSON.parse(result));
    });
  }, []);

  const fetchTags = async(result)=>{
    setLoaded(false)
    const GameScore = Parse.Object.extend("tagsOcean");
    const query = new Parse.Query(GameScore);
    query.equalTo("usuarioId", result.objectId);
    const results = await query.find();
    
    if(results.length)
        setTags(results)

    setLoaded(true)
        
  }

  const signIn = async()=>{
    setLoading(true);
    const GameScore = Parse.Object.extend("UserOcean");
    const query = new Parse.Query(GameScore);
    query.equalTo("email", email);
    query.equalTo("password", password);
    const results = await query.find();
    setLoading(false);
    if(results.length){
      await AsyncStorage.setItem('usuario', JSON.stringify(results[0]));
      setUser(results[0]);
    }else{
      Alert.alert('🔥', 'Error al iniciar sesión. Revisa los datos.');
    } 
  }

  const signUp = async()=>{

    const GameScore = Parse.Object.extend("UserOcean");
    const query = new Parse.Query(GameScore);
    query.equalTo("email", email);
    const results = await query.find();
    setLoading(true);
    if(results.length){
      Alert.alert('🔥', 'Ya existe un usuario con esta cuenta de correo.');
      setLoading(false);
    }else{
      const gameScore = new GameScore();
      gameScore.set("nombre", nombre);
      gameScore.set("apellido", apellido);
      gameScore.set("telefono", telefono);
      gameScore.set("email", email);
      gameScore.set("password", password);

    gameScore.save()
    .then((gameScore) => {
      Alert.alert('🎸', 'Te haz registrado con éxito');
      setLoading(false)
    }, (error) => {
      Alert.alert('🔥', 'Hubo un problema con tu registro');
      setLoading(false)
    });
    }
    
  }

  const logOut = async() => {
    await AsyncStorage.removeItem('usuario');
    setUser(null)
  }

  const selectData = (item)=>{
    setSelected(item)
    setModal2Visible(true)
  }

  if(!user){
    return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl
              onRefresh={fetchTags(user)}
            />
          }
          style={[styles.scrollView, styles.blackscreen]}>
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
              <View style={styles.userTypesContainer}>
                <ImageBackground style={styles.mark} source={require('./assets/logo.png')} >
                  
                </ImageBackground>
              </View>
              <Text style={styles.loginTitle}>Ingresa</Text>
              <Input
                value = {email}
                autoCapitalize = "none"
                keyboardType = "email-address"
                onChangeText={texto => setEmail( texto )}
                placeholder="Correo electrónico"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Input
                value = {password}
                secureTextEntry = {true}
                autoCapitalize = "none"
                onChangeText={texto => setPassword( texto )}
                placeholder="Contraseña"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Button
                loading={isLoading}
                title="Iniciar sesión"
                containerStyle={{ flex: -1 }}
                buttonStyle={styles.signUpButton}
                titleStyle={styles.signUpButtonText}
                onPress={signIn}
                disabled={isLoading}
                />
                <Button
                  title="No re haz registrado? Haz clic aquí!"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ setModalVisible(true) }}
                />
            </KeyboardAvoidingView>
        </ScrollView>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={modalVisible}
        >
          <Header
                containerStyle={{ backgroundColor: "black", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Crear cuenta</Text>}
                rightComponent={<Icon
                  clean
                  size = {40}
                  name='md-close-circle'
                  type='ionicon'
                  color="#ccc"
                  onPress={() => setModalVisible(false)} />}
              />
          
          <ScrollView>
            <KeyboardAvoidingView>
              <View style={styles.sectionContainer}>
              <Text style={{margin: 10, fontSize: 20}}>Todos los campos son obligatorios *</Text>
              {/* <Text style={styles.sectionDescription}> { decodeURIComponent(nombreproducto) } </Text> */}
              <Input
                value = {nombre}
                onChangeText={texto => setNombre( texto )}
                placeholder="Nombre"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Input
                value = {apellido}
                onChangeText={texto => setApellido( texto )}
                placeholder="Apellido"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Input
                value = {email}
                autoCapitalize = "none"
                keyboardType = "email-address"
                onChangeText={texto => setEmail( texto )}
                placeholder="Correo electrónico"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Input
                value = {telefono}
                keyboardType="phone-pad"
                onChangeText={texto => setTelefono( texto )}
                placeholder="Teléfono"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Input
                value = {password}
                autoCapitalize = "none"
                secureTextEntry = {true}
                onChangeText={texto => setPassword( texto )}
                placeholder="Contraseña"
                inputContainerStyle={styles.inputContainer}  
                inputStyle={styles.inputStyle}
                errorStyle={styles.errorInputStyle}
                autoCorrect={false}
                blurOnSubmit={false}
                placeholderTextColor="gray"
              />
              <Button
                loading={isLoading}
                title="¡Registrarse!"
                containerStyle={{ flex: -1, alignSelf: "center" }}
                buttonStyle={styles.signUpButton}
                titleStyle={styles.signUpButtonText}
                onPress={signUp}
                disabled={isLoading}
                />
                <Button
                  title="Cerrar"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ setModalVisible(false) }}
                />
            </View>
            </KeyboardAvoidingView>
            
          </ScrollView>
        </Modal>
            
      </SafeAreaView>
    </>
    );
  }else
  return (
    <AppNavigator
      screenProps={{ logOut: logOut }}
      ref={nav => {
        this.navigator = nav;
      }}
    />
  );
};

export default App;
