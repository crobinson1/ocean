/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  TextInput,
  ScrollView,
  View,
  Text,
  StatusBar,
  AsyncStorage,
  KeyboardAvoidingView,
  ImageBackground,
  Image,
  Modal,
  RefreshControl,
  Alert,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';

import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import styles from "./styles.css.js";
import { Input, Button, ListItem, Header, Avatar, Icon } from 'react-native-elements';
import Parse from "parse/react-native";

Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(
  "myAppIdOceanCr0bs",
  "crobskey",
  "b0t3c0m4st3rk3y"
);
Parse.serverURL = 'https://api.unidivisas.com/ocean';

const Inicio = () => {
  const [user, setUser] = useState( null );
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [telefono, setTelefono] = useState("");
  const [nombre, setNombre] = useState("");
  const [apellido, setApellido] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [loaded, setLoaded] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [selected, setSelected] = useState(null);
  const [tags, setTags] = useState([]);
  useEffect(() => {
    setLoading(false);
    AsyncStorage.getItem('usuario', (err, result) => {
      setUser(JSON.parse(result))
      fetchTags(JSON.parse(result));
    });
  }, []);

  const fetchTags = async(result)=>{
    setLoaded(false)
    const GameScore = Parse.Object.extend("tagsOcean");
    const query = new Parse.Query(GameScore);
    query.equalTo("usuarioId", result.objectId);
    const results = await query.find();
    
    if(results.length)
        setTags(results)

    setLoaded(true)
        
  }


  const selectData = (item)=>{
    setSelected(item)
    setModal2Visible(true)
  }
if(user)
  return (
    <>
      <Header
        containerStyle={{ backgroundColor: "black", height: 95 }}
        centerComponent={ <Text style={styles.headerTitle}>Mis Tags</Text>}
      />
      <SafeAreaView>
        <ImageBackground
            style={[styles.fixed, styles.imgContainter]}
            source={require('./assets/fondo.png')}
        />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          refreshControl={
            <RefreshControl
              refreshing={!loaded}
              onRefresh={()=>fetchTags(user)}
            />
          }
          style={styles.scrollView}>
          <View style={styles.body}>
            <View style={styles.userHeader}>
            <ImageBackground
                style={[styles.fixed, styles.imgHeader]}
                imageStyle={{ borderRadius: 30 }}
                source={require('./assets/nfc.png')}
            />
            <ImageBackground source={require('./assets/barra.png')} style={styles.barra}>
              <Text>{user.nombre} {user.apellido}</Text>
            </ImageBackground>
            </View>
            <View style={[styles.sectionContainer, {marginTop: 0}]}>
               <Text style={[styles.headerTitle, {color: "black"}]}>Tu código es: {user.objectId}</Text>
               <Text style={{color: "black", fontWeight: "bold"}}>Tus maletas con tags:</Text>
              {/* <View style={styles.rowIcon}> */}
              {tags.map((item, index) => {
                return (
                  <ListItem
                    containerStyle={{
                      backgroundColor: "#C4C4C4",
                      marginHorizontal: 16,
                      marginVertical: 8,
                      borderRadius: 40,
                      paddingLeft: 30
                    }}
                    // leftAvatar={{ rounded: false, source: require('./assets/iconoOcean.jpeg') }}
                    leftAvatar = {
                      <Icon
                        clean
                        size = {30}
                        name='md-briefcase'
                        type='ionicon'
                        color="#fff"/>
                    }
                    title={item.get("nombre")}
                    titleStyle={{ color: 'white', fontWeight: 'bold' }}
                    subtitleStyle={{ color: 'white' }}
                    subtitle={item.get("info")}
                    chevron={{ color: 'white' }}
                    onPress={()=>selectData(item)}
                  />
                // <View style={{alignItems: "center", width: "25%", justifyContent: "center"}}>
                //   <TouchableOpacity onPress={()=>selectData(item)}>
                //     <View style={styles.blackSquare}>
                //       <Image source={require('./assets/logo.png')} style={styles.miniLogo}  />
                //     </View>
                //   </TouchableOpacity>
                //   <Text style={styles.miniText}>{item.get("nombre")}</Text>
                // </View>
                );
              })}
              {/* </View> */}
            </View>
          </View>
        </ScrollView>
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={modal2Visible}
        >
          <Header
                containerStyle={{ backgroundColor: "black", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Tag</Text>}
                rightComponent={<Icon
                  clean
                  size = {40}
                  name='md-close-circle'
                  type='ionicon'
                  color="#ccc"
                  onPress={() => setModal2Visible(false)} />}
              />
          
          <ScrollView>
                <View style={styles.body}>
                    <View style={[
                        styles.sectionContainer, 
                        {
                            backgroundColor: "black", 
                            width: "80%", 
                            marginHorizontal: "10%",
                            padding: 30
                        }]}>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Nombre: 
                            {selected ? (" " + selected.get("nombre")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Teléfono:
                            {selected ? (" " + selected.get("telefono")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Correo electrónico:
                            {selected ? (" " + selected.get("email")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Código:
                            {selected ? (" " + selected.get("codigo")) : (null)}</Text>
                        </Text>
                        <Text style={[styles.sectionDescription]}>
                            <Text style={{ color: "white", fontSize: 17 }}>Información adicional:
                            {selected ? (" " + selected.get("info")) : (null)}</Text>
                        </Text>
                    </View>
                </View>
                
                <Button
                  title="Cerrar"
                  type="clear"
                  titleStyle ={styles.clearButton}
                  onPress={()=>{ setModal2Visible(false) }}
                />
            
          </ScrollView>
        </Modal>
      </SafeAreaView>
    </>
  );

  return (
    <View/>
  );
};

export default Inicio;
