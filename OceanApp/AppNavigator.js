/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * eslint-disable quotes, semi
 */
import React, { Component } from "react";
import { TouchableHighlight, Image } from "react-native";
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Input, Button, Icon } from "react-native-elements";
// import Metas from "./Metas";
// import Capacitaciones from "./Capacitaciones";
import Inicio from "./Inicio";
import Envio from "./Envio";
import Chat from "./Chat";


const TabNavigator = createBottomTabNavigator(
  {
    Home: Inicio,
    Chat: Chat,
    "Envío de equipaje": Envio,
    // Capacitaciones: Capacitaciones,
    // Bienestar: Bienestar,
    // Perfil: Perfil
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        // let IconComponent = Ionicons;
        let iconName;
        if (routeName === "Home") {
          iconName = "md-home";
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          // IconComponent = HomeIconWithBadge;
        } else if (routeName === "Crear") {
          iconName = "md-menu";
        } else if (routeName === "Envío de equipaje") {
          iconName = "ios-redo";
        } else if (routeName === "Capacitaciones") {
          iconName = "md-school";
        } else if (routeName === "Chat") {
            iconName = "md-chatbubbles";
        }
        // You can return any component that you like here!
        return <Icon  type="ionicon" name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "white",
      inactiveTintColor: "#ccc",
      style:{height:50, backgroundColor: "black"}
    }
  }
);

const AppNavigator = createAppContainer(TabNavigator)


export default AppNavigator
