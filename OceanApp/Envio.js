import React, { Component } from 'react';
import { 
    View, Text, SafeAreaView, ScrollView, ImageBackground, RefreshControl,
    KeyboardAvoidingView 
} from 'react-native';
import styles from "./styles.css.js";
import { Input, Button, CheckBox, Header, Avatar, Icon } from 'react-native-elements';
import Parse from "parse/react-native";
    
const Envio = () => {
    const [loaded, setLoaded] = React.useState(true);
    const [nombres, setNombres] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [telefono, setTelefono] = React.useState("");
    const [recogida, setRecogida] = React.useState("");
    const [observaciones, setObservaciones] = React.useState("");
    const [contacto, setContacto] = React.useState("");
    const [telcontacto, setTelcontacto] = React.useState("");
    const [checked, setChecked] = React.useState(false)
    const [isLoading, setLoading] = React.useState(false)
    const [usernameValid, setUsernameValid] = React.useState(true);
    
    React.useEffect(() => {
        if(checked){
            setContacto(nombres)
            setTelcontacto(telefono)
        }
      }, [checked]);
    signIn = () => {
        setLoading(true)
        const GameScore = Parse.Object.extend("sinmaletas");
        const gameScore = new GameScore();

        gameScore.set("nombres", nombres);
        gameScore.set("email", email);
        gameScore.set("celular", telefono);
        gameScore.set("dirRecogida", recogida);
        gameScore.set("observaciones", observaciones);
        gameScore.set("contacto", contacto);
        gameScore.set("celContacto", telcontacto);

        gameScore.save()
        .then((gameScore) => {
            setNombres("")
            setEmail("")
            setTelefono("")
            setRecogida("")
            setObservaciones("")
            setContacto("")
            setTelcontacto("")
            setChecked(false)
            setLoading(false);
            alert('Su solicitud fué enviada con éxito. A su correo llegará los detalles para el pago');
        }, (error) => {       
            setLoading(false); 
            alert('Falla al enviar los datos: ' + error.message);
        });

    }
    return (
        <>
            <Header
                containerStyle={{ backgroundColor: "black", height: 95 }}
                centerComponent={ <Text style={styles.headerTitle}>Envio de equipajes</Text>}
            />
            <SafeAreaView>
                <ImageBackground
                    style={[styles.fixed, styles.imgContainter]}
                    source={require('./assets/fondo.png')}
                />
                <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                refreshControl={
                    <RefreshControl
                    refreshing={!loaded}
                    onRefresh={()=>fetchTags(user)}
                    />
                }
                style={styles.scrollView}>
                    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                        <Text style={styles.sectionDescription}> Solicita el servicio de envío de tu equipaje al Aeropuerto </Text>
                        <Input
                            value = {nombres}
                            autoCapitalize = "words"
                            onChangeText={texto => setNombres( texto )}
                            placeholder="Nombre y Apellidos"
                            inputContainerStyle={styles.inputContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                            onSubmitEditing={() => {
                                setUsernameValid(nombres.length > 0)
                              }}
                              errorMessage={
                                usernameValid ? null : nombres.length ? null : "Este campo es obligatorio"
                              }
                        />
                        <Input
                            value = {email}
                            autoCapitalize = "none"
                            keyboardType = "email-address"
                            onChangeText={texto => setEmail( texto )}
                            placeholder="Correo electrónico"
                            inputContainerStyle={styles.inputContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                            onSubmitEditing={() => {
                                setUsernameValid(email.length > 0)
                              }}
                              errorMessage={
                                usernameValid ? null : email.length ? null : "Este campo es obligatorio"
                              }
                        />
                        <Input
                            value = {telefono}
                            autoCapitalize = "none"
                            keyboardType = "phone-pad"
                            onChangeText={texto => setTelefono( texto )}
                            placeholder="Número de celular"
                            inputContainerStyle={styles.inputContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                            onSubmitEditing={() => {
                                setUsernameValid(telefono.length > 0)
                              }}
                              errorMessage={
                                usernameValid ? null : telefono.length ? null : "Este campo es obligatorio"
                              }
                        />
                        <Text style={styles.sectionDescription}>¿En dónde recogemos tu equipaje?</Text>
                        <Input
                            value = {recogida}
                            autoCapitalize = "none"
                            onChangeText={texto => setRecogida( texto )}
                            placeholder="Dirección"
                            inputContainerStyle={styles.inputContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                            onSubmitEditing={() => {
                                setUsernameValid(recogida.length > 0)
                              }}
                              errorMessage={
                                usernameValid ? null : recogida.length ? null : "Este campo es obligatorio"
                              }
                        />
                        <Input
                            value = {observaciones}
                            autoCapitalize = "none"
                            multiline = {true}
                            numberOfLines={4}
                            onChangeText={texto => setObservaciones( texto )}
                            placeholder="Observaciones"
                            inputContainerStyle={styles.textareaContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                        />
                        <Text style={styles.sectionDescription}>Persona de contacto al momento de recoger tu equipaje</Text>
                        <CheckBox
                            checkedColor= "black"
                            checked={checked}
                            containerStyle={styles.inputContainer}  
                            title='Usar mis datos como persona de contacto'
                            onPress={() => setChecked(!checked)}
                        />
                        <Input
                            value = {contacto}
                            autoCapitalize = "words"
                            onChangeText={texto => setContacto( texto )}
                            placeholder="Nombre y Apellidos"
                            inputContainerStyle={styles.inputContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                            onSubmitEditing={() => {
                                setUsernameValid(contacto.length > 0)
                              }}
                              errorMessage={
                                usernameValid ? null : contacto.length ? null : "Este campo es obligatorio"
                              }
                        />
                        <Input
                            value = {telcontacto}
                            autoCapitalize = "none"
                            keyboardType = "phone-pad"
                            onChangeText={texto => setTelcontacto( texto )}
                            placeholder="Número de celular de la persona de contacto"
                            inputContainerStyle={styles.inputContainer}  
                            inputStyle={styles.inputStyleBlack}
                            errorStyle={styles.errorInputStyle}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            placeholderTextColor="gray"
                            onSubmitEditing={() => {
                                setUsernameValid(telcontacto.length > 0)
                              }}
                              errorMessage={
                                usernameValid ? null : telcontacto.length ? null : "Este campo es obligatorio"
                              }
                        />
                        <Button
                            loading={isLoading}
                            title="Continuar"
                            containerStyle={{ flex: -1 }}
                            buttonStyle={styles.signUpButton}
                            titleStyle={styles.signUpButtonText}
                            onPress={signIn}
                            disabled={isLoading}
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}
export default Envio;